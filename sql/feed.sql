CREATE FUNCTION feed() RETURNS TABLE
(
	id            BIGINT,
	loses         INTEGER,
	ties          INTEGER,
	wins          INTEGER,
	total         INTEGER,
	elo           INTEGER,
	"fedIndex"    DOUBLE PRECISION,
	"feederIndex" DOUBLE PRECISION,
	banned        BOOLEAN
)
LANGUAGE plpgsql
AS $$
DECLARE
	feed_slope REAL;
BEGIN
	feed_slope := (
		SELECT -1 / (percentile_cont(0.5) WITHIN GROUP(ORDER BY turn) - 2)
		FROM "Game"
		WHERE status IN (3, 4, 6)
			AND "player0Id" IS NOT NULL
			AND "player1Id" IS NOT NULL
			AND "player0Id" NOT IN (SELECT "userId" FROM "Ban")
			AND "player1Id" NOT IN (SELECT "userId" FROM "Ban")
	);
	CREATE TEMPORARY TABLE "GameFeed" AS (
		SELECT
			1 + (turn - 2) * feed_slope AS "feedIndex",
			CASE WHEN status IN (4, 5) THEN "player0Id" ELSE "player1Id" END AS "winnerId",
			CASE WHEN status IN (4, 5) THEN "player1Id" ELSE "player0Id" END AS "looserId"
		FROM "Game"
		WHERE status >= 4
	);
	RETURN QUERY
		SELECT
			"Player".id,
			"Player".loses,
			"Player".ties,
			"Player".wins,
			"Player".loses + "Player".ties + "Player".wins,
			"Player".elo,
			coalesce(T1."fedIndex", 0),
			coalesce(T2."feederIndex", 0),
			"Player".id IN (SELECT "userId" FROM "Ban")
		FROM "Player"
			FULL OUTER JOIN (
				SELECT
					"winnerId" AS id,
					avg("feedIndex") AS "fedIndex"
				FROM "GameFeed"
				GROUP BY "winnerId"
			) T1 ON T1.id = "Player".id
			FULL OUTER JOIN (
				SELECT
					"looserId" AS id,
					avg("feedIndex") AS "feederIndex"
				FROM "GameFeed"
				GROUP BY "looserId"
			) T2 ON T2.id = "Player".id
		WHERE "Player".id IS NOT NULL
			AND "Player".loses + "Player".ties + "Player".wins >= 20;
	DROP TABLE "GameFeed";
END $$
