FROM node:current-alpine
WORKDIR /home/node
COPY --chown=node:node ./app/ ./
COPY ./cli /usr/local/bin/
RUN chmod 755 /usr/local/bin/cli
USER node
ENV NODE_ENV=production
EXPOSE 80
CMD node --enable-source-maps build/main.js
