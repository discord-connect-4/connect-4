Terms of service of the Connect 4 Discord bot
-------------------------------------------------------------------------------
publication date: 2022-10-22

Using the bot in a way that was not intended by the developer, if it can be
detrimental to other users, is prohibited. This includes, among other things,
the use of a program to gain an unfair advantage over your opponents, or the
use of win trading to artificially inflate your placement on the leaderboard.

If this is detected, it will lead to your account being permanently banned from
using the bot.
