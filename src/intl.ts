import { createIntl, createIntlCache, IntlCache, IntlShape } from '@formatjs/intl'
import { mkdir, readdir, readFile, writeFile } from 'fs/promises'
import superagent from 'superagent'
import yaml from 'yaml'
import { mainLogger } from './logger'
import { exec, parallelForEach, PrimitiveType } from './utils'

type NestedDictionary = { [key: string]: string | NestedDictionary }
type IntlObjects = Record<string, {
	translationProgress: number,
	obj: IntlShape,
}>

let cache: IntlCache
let intlObjects: IntlObjects = {}

export const fallbackLocale = 'en-US'

export async function initIntl() {
	// add the source language as a translation
	let source = await readFile('./intl/source.yml', 'utf8')
	source = yaml.parse(source)
	source = JSON.stringify(source, null, '\t') + '\n'
	await writeFile(`./intl/strings/${fallbackLocale}.json`, source)

	const newCache = createIntlCache()
	const newIntlObjects: IntlObjects = {}

	await parallelForEach(await readdir('./intl/strings'), async file => {
		const locale = file.substring(0, file.lastIndexOf('.'))
		const rawContent = await readFile('./intl/strings/' + file, 'utf8')
		const strings = flatten(JSON.parse(rawContent) as NestedDictionary)

		const stringsValues = Object.values(strings)
		const translationProgress = stringsValues.filter(str => str !== '').length / stringsValues.length

		if (translationProgress < 0.1) {
			return
		}

		newIntlObjects[locale] = {
			translationProgress,
			obj: createIntl({
				locale,
				messages: strings,
				onError: e => mainLogger.error(e),
			}, newCache)
		}
	})

	cache = newCache
	intlObjects = newIntlObjects
}

export function getLocales(): string[] {
	return Object.keys(intlObjects)
}

export function getLocaleName(locale: string): string {
	return (intlObjects[locale].obj.messages.locale_name as string) || locale
}

export function translate(locale: string, key: string, options?: Record<string, PrimitiveType> | number): string {
	key = resolveKey(key)

	if (locale !== fallbackLocale && !intlObjects[locale].obj.messages[key]) {
		return translate(fallbackLocale, key, options)
	}

	if (typeof options === 'number') {
		options = {
			count: options,
		}
	}

	return intlObjects[locale].obj.formatMessage({ id: key }, options)
}

export function translateIntoFallback(key: string, options?: Record<string, PrimitiveType> | number): string {
	return translate(fallbackLocale, key, options)
}

export function getTranslationProgress(locale: string): number {
	return intlObjects[locale].translationProgress
}

function flatten(dic: NestedDictionary): Record<string, string> {
	const res: Record<string, string> = {}
	for (const key in dic) {
		const val = dic[key]
		if (typeof val === 'string') {
			res[key] = val
		} else {
			let subDic = flatten(val)
			for (const nestedKey in subDic) {
				res[`${key}.${nestedKey}`] = subDic[nestedKey]
			}
		}
	}
	return res
}

/** handle keys that will be renamed (temporally replace the new name by the old name) */
function resolveKey(key: string): string {
	switch (key) {
		case 'cmd.server-conf.lang.translation_progress': return 'dashboard.translation_progress'
		case 'cmd.server-conf.lang.translation_contribution': return 'dashboard.translation_contribution'
		case 'cmd.server-conf.img.win': return 'dashboard.win_img'
		case 'cmd.server-conf.img.tie': return 'dashboard.tie_img'
		default: return key
	}
}
