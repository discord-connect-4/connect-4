import { readFileSync } from 'fs'
import { resolve } from 'path'
import { parse } from 'yaml'
import Ajv, { JSONSchemaType } from 'ajv'

interface Config {
	dev_mode: boolean
	log_level: 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal'
	bot_token: string
	default_game_over_img: {
		win: string
		tie: string
	}
	database: {
		host: string
		port: number
		name: string
		username: string
		password: string
		sync: boolean
		log: boolean
	}
	support_server: false | {
		invite_code: string
	}
	topgg: false | {
		token: string
		webhook_auth: string
	}
}

const configSchema: JSONSchemaType<Config> = {
	type: 'object',
	properties: {
		dev_mode: { type: 'boolean' },
		log_level: {
			type: 'string',
			enum: ['trace', 'debug', 'info', 'warn', 'error', 'fatal'],
		},
		bot_token: { type: 'string' },
		default_game_over_img: {
			type: 'object',
			properties: {
				win: { type: 'string' },
				tie: { type: 'string' },
			},
			required: ['win', 'tie'],
			additionalProperties: false,
		},
		database: {
			type: 'object',
			properties: {
				host: { type: 'string' },
				port: { type: 'number' },
				name: { type: 'string' },
				username: { type: 'string' },
				password: { type: 'string' },
				sync: { type: 'boolean' },
				log: { type: 'boolean' },
			},
			required: [ 'host', 'port', 'name', 'username', 'password', 'sync', 'log' ],
			additionalProperties: false,
		},
		support_server: {
			anyOf: [
				{
					type: 'boolean',
					const: false,
				},
				{
					type: 'object',
					properties: {
						invite_code: { type: 'string' },
					},
					required: [ 'invite_code' ],
					additionalProperties: false,
				},
			],
		},
		topgg: {
			anyOf: [
				{
					type: 'boolean',
					const: false,
				},
				{
					type: 'object',
					properties: {
						token: { type: 'string' },
						webhook_auth: { type: 'string' },
					},
					required: [ 'token', 'webhook_auth' ],
					additionalProperties: false,
				},
			],
		},
	},
	required: [ 'dev_mode', 'log_level', 'bot_token', 'default_game_over_img', 'database', 'support_server', 'topgg' ],
	additionalProperties: false,
}

const rawConfig = parse(readFileSync(resolve(__dirname, '../config.yml'), 'utf8'))

if (rawConfig.dev_mode === undefined) {
	rawConfig.dev_mode = process.env.NODE_ENV === 'development'
}

if (rawConfig.log_level === undefined) {
	rawConfig.log_level = rawConfig.dev_mode ? 'debug' : 'info'
}

if (typeof rawConfig.database == 'object' && rawConfig.database.log === undefined) {
	rawConfig.database.log = rawConfig.dev_mode
}

const validate = new Ajv().compile(configSchema)
if (!validate(rawConfig)) {
	console.error(validate.errors)
	throw new Error('Failed to validate the configuration.')
}

export const config: Config = rawConfig
