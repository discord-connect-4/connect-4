import { SlashCommandUserOption } from '@discordjs/builders'
import { client } from '../../bot'
import { BtnContext, CmdContext, Context } from '../../context'
import { ApplicationCommandOptionType, MessageFlags } from '../../discordApiTypes'
import { Game } from '../../orm/entities/Game'
import { Command, CommandType } from '../command'

const quota = 100

const command: Command = {
	name: 'play',
	type: CommandType.GAME,
	usableIfBanned: false,
	requireExternalEmojis: true,

	buildOptions(builder, t) {
		builder.addUserOption(
			new SlashCommandUserOption()
				.setName('opponent')
				.setDescription(t('cmd.play.opponent_description'))
				.setRequired(false)
		)
	},

	async exec(ctx: CmdContext): Promise<void> {
		const canPlay = await checkQuota(ctx)
		if (!canPlay) {
			ctx.logger.warn('user %s reached the quota limit', ctx.userId)
			await client.interactionReply(ctx, {
				content: ctx.p.t('cmd.play.quota', { smart_count: quota }),
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		const opponentOpt = ctx.interaction.data.options?.find(o => o.name === 'opponent')
		const opponent = opponentOpt !== undefined && opponentOpt.type === ApplicationCommandOptionType.User
			? await client.fetchUser(opponentOpt.value)
			: null

		if (opponent?.bot) {
			await client.interactionReply(ctx, {
				content: ctx.p.t('cmd.play.opponent_is_bot'),
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		const game = await Game.createFromPlayerIds(ctx.userId, opponent ? opponent.id : null, ctx)
		await client.interactionReply(ctx, await game.asInteractionResponse(ctx))
	},

	async onBtnClicked(ctx: BtnContext): Promise<void> {
		const match = /^game(\d+)_turn(\d+)_(play(?=.)|bell$|cancel$|forfeit$)([0-6]$)?/.exec(ctx.interaction.data.custom_id)
		if (!match) {
			throw new Error('failed to parse button customId: ' + ctx.interaction.data.custom_id)
		}

		const userId = ctx.userId
		const gameId = parseInt(match[1], 10)
		const turn = parseInt(match[2], 10)
		const action = match[3] as 'play' | 'bell' | 'cancel' | 'forfeit'

		const game = await ctx.tx.findOne(Game, gameId)
		if (!game) {
			throw new Error('game not found')
		}

		if (action === 'bell') {
			let msg: string

			if (game.isSubscribed(userId)) {
				await game.unsubscribe(ctx)
				msg = ctx.p.t('subscription.common.off')
			} else {
				const isPlayer = game.isPlayer(userId)
				if (isPlayer && game.player0 === game.player1) {
					msg = ctx.p.t('subscription.player.error_self')
				} else {
					await game.subscribe(ctx)
					msg = ctx.p.t( `subscription.${isPlayer ? 'player' : 'spectator'}.on`)
				}
			}

			await client.interactionReply(ctx, {
				content: msg,
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		let response = await (() => {
			// check if embed contains the current game state
			if (game.turn !== turn) {
				return null
			}

			// get the appropriate response to the interaction, or null if the action is invalid for the given user, game state or column
			switch (action) {
			case 'play':
				const column = parseInt(match[4], 10)
				return game.play(column, ctx)

			case 'cancel':
				return game.voteCancelTurn(ctx)

			case 'forfeit':
				return game.forfeit(ctx)
			}
		})()

		// if the response is null, it's either because:
		// - the embed doesn't contain the current game state -> refresh needed
		// - a button that shouldn't have been clicked, has been clicked -> refresh needed
		// - a user who isn't in the game or a player who isn't his turn to play, has tried to play -> do nothing... so refresh needed because Discord API isn't happy when we ignore the interaction
		if (response === null) {
			response = await game.asInteractionResponse(ctx)
		}

		await client.componentInteractionUpdateMsg(ctx.interaction, response)
	},
}

async function checkQuota(ctx: Context): Promise<boolean> {
	const count = await ctx.tx.createQueryBuilder()
		.select()
		.from(Game, 'Game')
		.where('"player0Id" = :playerId AND "startDatetime" >= :minDate', {
			playerId: ctx.userId,
			minDate: new Date(Date.now() - 86400000), // 24h ago
		})
		.getCount()

	return count < quota
}

export default command
