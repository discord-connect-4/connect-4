import { EmbedBuilder, time, TimestampStyles } from '@discordjs/builders'
import { client } from '../../bot'
import { BtnContext, CmdContext, Context } from '../../context'
import { APIInteractionResponseCallbackData, MessageFlags, Snowflake } from '../../discordApiTypes'
import { Game, Status } from '../../orm/entities/Game'
import { getDisplayNames } from '../../utils'
import { Command, CommandType, createPagination, getPageIndexFromBtnId } from '../command'

const maxListSize = 6

const command: Command = {
	name: 'list',
	type: CommandType.GAME,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		await client.interactionReply(ctx, await getPage(0, ctx))
	},

	async onBtnClicked(ctx: BtnContext): Promise<void> {
		const pageIndex = getPageIndexFromBtnId(ctx.interaction.data.custom_id)
		await client.componentInteractionUpdateMsg(ctx.interaction, await getPage(pageIndex, ctx))
	},
}

async function getPage(pageIndex: number, ctx: Context): Promise<APIInteractionResponseCallbackData> {
	const totalRowCount = await ctx.tx.createQueryBuilder()
		.select()
		.from(Game, 'Game')
		.where('status = :status', { status: Status.BOTH_PLAYERS_PLAYED })
		.andWhere('"player0Id" IS NOT NULL')
		.andWhere('"player1Id" IS NOT NULL')
		.andWhere('("player0Id" = :playerId OR "player1Id" = :playerId)', { playerId: ctx.userId })
		.getCount()

	const nbOfPage = Math.ceil(totalRowCount / maxListSize)
	if (nbOfPage === 0) {
		return {
			content: ctx.p.t('cmd.list.no_games'),
			flags: MessageFlags.Ephemeral,
		}
	}

	if (pageIndex >= nbOfPage) {
		pageIndex = nbOfPage - 1
	}

	const visibleRows = await ctx.tx.createQueryBuilder()
		.select('id')
		.addSelect(`CASE WHEN "player0Id" = ${ctx.userId} THEN "player1Id" ELSE "player0Id" END`, 'opponentId')
		.addSelect(`CASE WHEN (turn % 2) = 0 THEN "player0Id" = ${ctx.userId} ELSE "player1Id" = ${ctx.userId} END`, 'ownTurn')
		.addSelect('"lastMoveDatetime"')
		.from(Game, 'Game')
		.where('status = :status', { status: Status.BOTH_PLAYERS_PLAYED })
		.andWhere('"player0Id" IS NOT NULL')
		.andWhere('"player1Id" IS NOT NULL')
		.andWhere('("player0Id" = :playerId OR "player1Id" = :playerId)', { playerId: ctx.userId })
		.orderBy('"lastMoveDatetime"', 'DESC')
		.skip(maxListSize * pageIndex)
		.take(maxListSize)
		.getRawMany<{ id: number, opponentId: Snowflake, ownTurn: string, lastMoveDatetime: Date }>()

	const embed = new EmbedBuilder().setTitle(ctx.p.t('cmd.list.embed.title' ))
	const displayNames = await getDisplayNames(visibleRows.map(row => row.opponentId), ctx)

	embed.setFields(visibleRows.map(row => {
		const gameId = row.id
		const opponentDisplayName = displayNames[row.opponentId]
		const ownTurn = row.ownTurn
		const lastMoveDatetime = row.lastMoveDatetime

		return {
			name: '[ID ' + gameId + '] ' + time(lastMoveDatetime, TimestampStyles.ShortDateTime),
			value:
				ctx.p.t(`cmd.list.embed.${ownTurn ? 'your_turn' : 'his_turn'}`, {
					opponent: opponentDisplayName,
					separator: ownTurn ? '🔹' : '🔸'
				}) + '\n➖',
		}
	}))

	return {
		embeds: [embed.toJSON()],
		components: nbOfPage > 1 ? [createPagination(nbOfPage, pageIndex)] : [],
		flags: MessageFlags.Ephemeral,
	}
}

export default command
