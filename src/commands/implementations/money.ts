import { EmbedBuilder } from '@discordjs/builders'
import { client } from '../../bot'
import { config } from '../../config'
import { CmdContext } from '../../context'
import { APIEmbedField, MessageFlags } from '../../discordApiTypes'
import { Player } from '../../orm/entities/Player'
import { formatNumber } from '../../utils'
import { Command, CommandType } from '../command'

const command: Command = {
	name: 'money',
	type: CommandType.CUSTOMIZATION,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		const player = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)

		const fields: APIEmbedField[] = [
			{
				name: ctx.p.t('cmd.money.embed.wallet'),
				value: '🪙 ' + formatNumber(player.coins),
			},
			{
				name: ctx.p.t('cmd.money.embed.tip1.title'),
				value: ctx.p.t('cmd.money.embed.tip1.text'),
			},
		]

		if (config.topgg) {
			fields.push({
				name: ctx.p.t('cmd.money.embed.tip2.title'),
				value: ctx.p.t('cmd.money.embed.tip2.text', { voteUrl: `https://top.gg/bot/${client.user.id}` }),
			})
		}

		const embed = new EmbedBuilder()
			.setTitle('💵 💵 💵')
			.setFields(fields)

		await client.interactionReply(ctx, {
			embeds: [embed.toJSON()],
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
