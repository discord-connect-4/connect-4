import { EmbedBuilder } from '@discordjs/builders'
import { client } from '../../bot'
import { BtnContext, CmdContext, Context } from '../../context'
import { APIInteractionResponseCallbackData, MessageFlags } from '../../discordApiTypes'
import {
	Command,
	CommandType,
	createPagination,
	getCommandSynopsis,
	getPageIndexFromBtnId,
	nbOfCommandType
} from '../command'
import { getCommands } from '../manager'

const command: Command = {
	name: 'help',
	type: CommandType.UTILITY,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		await client.interactionReply(ctx, getPage(0, ctx))
	},

	async onBtnClicked(ctx: BtnContext): Promise<void> {
		const pageIndex = getPageIndexFromBtnId(ctx.interaction.data.custom_id)
		if (pageIndex >= nbOfCommandType) {
			throw new Error(`help page index doesn\'t exist: ${pageIndex} (max ${nbOfCommandType})`)
		}

		await client.componentInteractionUpdateMsg(ctx.interaction, getPage(pageIndex, ctx))
	},
}

function getPage(pageIndex: number, ctx: Context): APIInteractionResponseCallbackData {
	let content = getUsages(pageIndex, ctx)
	if (pageIndex === CommandType.MANAGEMENT) {
		content = ctx.p.t('cmd.help.embed.management_info') + '\n' + content
	}

	return {
		embeds: [
			new EmbedBuilder()
				.setTitle(ctx.p.t('cmd.help.embed.title'))
				.setFields([
					{
						name: getTitle(pageIndex, ctx),
						value: content,
					},
				])
				.toJSON(),
		],
		components: [
			createPagination(nbOfCommandType, pageIndex),
		],
		flags: MessageFlags.Ephemeral,
	}
}

function getTitle(type: CommandType, ctx: Context): string {
	let key: string

	switch (type) {
		case CommandType.GAME:
			key = 'game'
			break

		case CommandType.UTILITY:
			key = 'utility'
			break

		case CommandType.CUSTOMIZATION:
			key = 'customization'
			break

		case CommandType.MANAGEMENT:
			key = 'management'
			break
	}

	return ctx.p.t(`cmd.help.embed.section.${key}`)
}

function getUsages(type: CommandType, ctx: Context): string {
	return getCommands(type)
		.map(c => '➖\n' + getUsage(c, ctx))
		.join('\n')
}

function getUsage(command: Command, ctx: Context): string {
	return getCommandSynopsis(command.name, ctx) + '\n' +ctx. p.t(`cmd.${command.name}.description`).trim()
}

export default command
