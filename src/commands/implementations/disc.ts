import { ButtonBuilder, EmbedBuilder } from '@discordjs/builders'
import { client } from '../../bot'
import { BtnContext, CmdContext, Context } from '../../context'
import { Disc as DiscObj, discs, emojiForBtn } from '../../disc'
import { APIInteractionResponseCallbackData, ButtonStyle, MessageFlags } from '../../discordApiTypes'
import { Player } from '../../orm/entities/Player'
import { Command, CommandType, organiseButtons } from '../command'

const command: Command = {
	name: 'disc',
	type: CommandType.CUSTOMIZATION,
	usableIfBanned: true,
	requireExternalEmojis: true,

	async exec(ctx: CmdContext): Promise<void> {
		const player = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)
		await client.interactionReply(ctx, execStep0(player, ctx))
	},

	async onBtnClicked(ctx: BtnContext): Promise<void> {
		const match = /^disc([01])_choice(\d+)$/.exec(ctx.interaction.data.custom_id)
		if (!match) {
			throw new Error('failed to parse button customId: ' + ctx.interaction.data.custom_id)
		}
		const player = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)
		const choice = parseInt(match[2], 10)
		const response = await (match[1] === '0' ? execStep1 : execStep2)(player, choice, ctx)
		await ctx.tx.save(player)
		await client.componentInteractionUpdateMsg(ctx.interaction, response)
	},
}

function execStep0(player: Player, ctx: Context): APIInteractionResponseCallbackData {
	const availableDiscs = getAvailableDiscs(player)

	return {
		embeds: [
			new EmbedBuilder()
				.setTitle(ctx.p.t('cmd.disc.embed.title'))
				.setDescription(ctx.p.t('cmd.disc.embed.description0'))
				.setFields([
					{
						name: ctx.p.t('cmd.disc.embed.current_main'),
						value: discs[player.mainDisc].emoji,
					},
				])
				.toJSON()
		],
		components: organiseButtons(availableDiscs.map(d =>
			new ButtonBuilder()
				.setCustomId('disc0_choice' + d.index)
				.setStyle(ButtonStyle.Primary)
				.setEmoji(emojiForBtn(d))
		)),
		flags: MessageFlags.Ephemeral,
	}
}

async function execStep1(player: Player, choice: number, ctx: Context): Promise<APIInteractionResponseCallbackData> {
	const availableDiscs = getAvailableDiscs(player)

	// check if disk is available
	if (!availableDiscs.includes(discs[choice])) {
		// invalid state (desynchronized) -> go back to step 0
		return execStep0(player, ctx)
	}

	// modify secondary disc if identical
	if (player.secondaryDisc === choice) {
		player.secondaryDisc = player.mainDisc
	}

	// set the new disc
	player.mainDisc = choice

	const currentMain = discs[player.mainDisc]
	const currentSecondary = discs[player.secondaryDisc]
	const availableSecondary = availableDiscs.filter(d => d !== currentMain)

	const embed = new EmbedBuilder()
		.setTitle(ctx.p.t('cmd.disc.embed.title'))
		.setDescription(ctx.p.t('cmd.disc.embed.description1'))
		.setFields([
			{
				name: ctx.p.t('cmd.disc.embed.current_main'),
				value: currentMain.emoji,
			},
			{
				name: ctx.p.t('cmd.disc.embed.current_secondary'),
				value: currentSecondary.emoji,
			},
		])

	return {
		embeds: [embed.toJSON()],
		components: organiseButtons(availableSecondary.map(d =>
			new ButtonBuilder()
				.setCustomId('disc1_choice' + d.index)
				.setStyle(ButtonStyle.Primary)
				.setEmoji(emojiForBtn(d))
		)),
		flags: MessageFlags.Ephemeral,
	}
}

async function execStep2(player: Player, choice: number, ctx: Context): Promise<APIInteractionResponseCallbackData> {
	const currentMain = discs[player.mainDisc]
	const availableSecondary = getAvailableDiscs(player).filter(d => d !== currentMain)

	// check if disk is available
	if (!availableSecondary.includes(discs[choice])) {
		// invalid state (desynchronized) -> go back to step 0
		return execStep0(player, ctx)
	}

	// set the new disc
	player.secondaryDisc = choice
	const currentSecondary = discs[player.secondaryDisc]

	return {
		embeds: [
			new EmbedBuilder()
				.setTitle(ctx.p.t('cmd.disc.embed.title'))
				.setFields([
					{
						name: ctx.p.t('cmd.disc.embed.current_main'),
						value: currentMain.emoji,
					},
					{
						name: ctx.p.t('cmd.disc.embed.current_secondary'),
						value: currentSecondary.emoji,
					},
				])
				.toJSON()
		],
		components: [],
		flags: MessageFlags.Ephemeral,
	}
}

function getAvailableDiscs(player: Player): DiscObj[] {
	const available: DiscObj[] = []

	for (let i = 0; i < discs.length; ++i) {
		if (player.discs >> i & 1) available.push(discs[i])
	}

	return available
}

export default command
