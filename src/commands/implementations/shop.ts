import { bold, ButtonBuilder, EmbedBuilder } from '@discordjs/builders'
import { client } from '../../bot'
import { BtnContext, CmdContext, Context } from '../../context'
import { Currency, discs, emojiForBtn } from '../../disc'
import { APIInteractionResponseCallbackData, ButtonStyle, MessageFlags } from '../../discordApiTypes'
import { Player } from '../../orm/entities/Player'
import { formatNumber } from '../../utils'
import { Command, CommandType, organiseButtons } from '../command'

const command: Command = {
	name: 'shop',
	type: CommandType.CUSTOMIZATION,
	usableIfBanned: true,
	requireExternalEmojis: true,

	async exec (ctx: CmdContext): Promise<void> {
		const player = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)
		await client.interactionReply(ctx, constructResponse(player, ctx))
	},

	async onBtnClicked(ctx: BtnContext): Promise<void> {
		const match = /^buy_(\d+)$/.exec(ctx.interaction.data.custom_id)
		if (!match) {
			throw new Error('failed to parse button customId: ' + ctx.interaction.data.custom_id)
		}

		const disc = discs[parseInt(match[1], 10)]
		if (!disc) {
			throw new Error('disc not found: ' + match[1])
		}

		const player = await Player.createIfDoesNotExist(ctx.userId, ctx.tx)
		if (player.buyDisc(disc)) {
			await ctx.tx.save(player)
		}

		await client.componentInteractionUpdateMsg(ctx.interaction, constructResponse(player, ctx))
	},
}

function constructResponse(player: Player, ctx: Context): APIInteractionResponseCallbackData {
	let text = ''
		+ bold(ctx.p.t('cmd.shop.embed.wallet')) + '\n'
		+ `🪙 ${formatNumber(player.coins)}\n`
		+ (player.gems === 0 ? '' : `💎 ${formatNumber(player.gems)}\n`)
		+ '\n'
		+ bold(ctx.p.t('cmd.shop.embed.howto_coins.title')) + '\n'
		+ `${ctx.p.t('cmd.shop.embed.howto_coins.text', { topggUrl: `https://top.gg/bot/${client.user.id}` })}\n`
		+ '\n'
		+ bold(ctx.p.t('cmd.shop.embed.items')) + '\n'

	for (const disc of discs) {
		let currency: string
		let symbol: string
		let amount: number

		switch (disc.currency) {
		case Currency.COIN:
			currency = ctx.p.t('cmd.shop.coins')
			symbol = '🪙'
			amount = player.coins
			break

		case Currency.GEM:
			if (player.gems === 0) {
				continue
			}
			currency = ctx.p.t('cmd.shop.gems')
			symbol = '💎'
			amount = player.gems
			break

		case Currency.PRICELESS:
			continue
		}

		const status = (() => {
			if (player.haveDisc(disc)) {
				return 'sold'
			} else if (disc.price > amount) {
				return 'insufficient_coins'
			} else {
				return 'available'
			}
		})()

		text += ctx.p.t('cmd.shop.embed.' + status, {
			currency: currency,
			symbol: symbol,
			disc: disc.emoji,
			price: formatNumber(disc.price),
		}) + '\n'
	}

	const embed = new EmbedBuilder()
		.setTitle(ctx.p.t('cmd.shop.embed.title'))
		.setDescription(text)

	const buttons = discs
		.filter(disc => !player.haveDisc(disc) && disc.currency !== Currency.PRICELESS && (disc.currency !== Currency.GEM || player.gems !== 0))
		.map(disc => new ButtonBuilder()
			.setCustomId('buy_' + disc.index)
			.setStyle(ButtonStyle.Primary)
			.setEmoji(emojiForBtn(disc))
			.setDisabled(disc.price > (disc.currency === Currency.COIN ? player.coins : player.gems))
		)

	return {
		embeds: [embed.toJSON()],
		components: organiseButtons(buttons),
		flags: MessageFlags.Ephemeral,
	}
}

export default command
