import { SlashCommandIntegerOption } from '@discordjs/builders'
import { client } from '../../bot'
import { CmdContext } from '../../context'
import { MessageFlags } from '../../discordApiTypes'
import { checkGameArg, Command, CommandType } from '../command'
import play from './play'

const command: Command = {
	name: 'resume',
	type: CommandType.GAME,
	usableIfBanned: false,
	requireExternalEmojis: true,

	buildOptions(builder, t) {
		builder.addIntegerOption(
			new SlashCommandIntegerOption()
				.setName('game_id')
				.setDescription(t('cmd.resume.game_id_description'))
				.setRequired(true)
		)
	},

	async exec(ctx: CmdContext): Promise<void> {
		const { game, error } = await checkGameArg(ctx)
		if (error !== null) {
			await client.interactionReply(ctx, {
				content: error,
				flags: MessageFlags.Ephemeral,
			})
			return
		}

		await client.interactionReply(ctx, await game.asInteractionResponse(ctx))
	},

	onBtnClicked: play.onBtnClicked,
}

export default command
