import { EmbedBuilder, escapeMarkdown, italic, SlashCommandSubcommandBuilder } from '@discordjs/builders'
import { sort } from 'timsort'
import { client } from '../../bot'
import { CmdContext } from '../../context'
import { APIEmbed, APIInteractionResponseCallbackData, MessageFlags, Snowflake } from '../../discordApiTypes'
import { fromRankedPlayers, Player } from '../../orm/entities/Player'
import { compareIds, getDisplayNames, intersectSorted, mentionCommand } from '../../utils'
import { Command, CommandType } from '../command'

interface PlayerRankInfo {
	id: Snowflake
	elo: number
	anonymous: boolean
	rank: number
}

const globalSubCmdName = 'global'
const guildSubCmdName = 'server'

const command: Command = {
	name: 'lead',
	type: CommandType.GAME,
	usableIfBanned: true,
	requireExternalEmojis: false,

	buildOptions(builder, t) {
		builder.addSubcommand(
			new SlashCommandSubcommandBuilder()
				.setName(globalSubCmdName)
				.setDescription(t('cmd.lead.global.description'))
		)
		builder.addSubcommand(
			new SlashCommandSubcommandBuilder()
				.setName(guildSubCmdName)
				.setDescription(t('cmd.lead.guild.description'))
		)
	},

	async exec(ctx: CmdContext): Promise<void> {
		await client.slashCmdInteractionDeferReply(ctx.interaction, { flags: MessageFlags.Ephemeral })

		const res = ctx.interaction.data.options?.find(o => o.name === globalSubCmdName)
			? await buildGlobalLeaderboard(ctx)
			: await buildGuildLeaderboard(ctx)

		await client.interactionEditReply(ctx, res)
	}
}

async function buildGlobalLeaderboard(ctx: CmdContext): Promise<APIInteractionResponseCallbackData> {
	const players = await getLeadPlayers(ctx)

	if (players.length === 0) {
		return {
			content: ctx.p.t('cmd.lead.global.no_ranked'),
			flags: MessageFlags.Ephemeral,
		}
	}

	const embedTitle = ctx.p.t('cmd.lead.global.embed_title')

	return {
		embeds: [await buildEmbed(embedTitle, players, ctx)],
		flags: MessageFlags.Ephemeral,
	}
}

async function buildGuildLeaderboard(ctx: CmdContext): Promise<APIInteractionResponseCallbackData> {
	if (ctx.guild === null) {
		return {
			content: ctx.p.t('cmd.lead.guild.not_in_guild', {
				lead_global_cmd: mentionCommand(command.name, globalSubCmdName)
			}),
			flags: MessageFlags.Ephemeral,
		}
	}

	const memberIds = (await client.fetchGuildMembers(ctx.guild.id)).map(m => m.user.id)
	sort(memberIds, compareIds)

	const playerIds = (await ctx.tx.createQueryBuilder()
		.select('id')
		.from(Player, 'Player')
		.orderBy('id')
		.getRawMany<{ id: Snowflake }>())
		.map(p => p.id)

	const filterIds = intersectSorted(memberIds, playerIds, compareIds)
	const leadPlayers = await getLeadPlayers(ctx, filterIds)

	if (leadPlayers.length === 0) {
		return {
			content: ctx.p.t('cmd.lead.guild.no_ranked'),
			flags: MessageFlags.Ephemeral,
		}
	}

	const embedTitle = ctx.p.t('cmd.lead.guild.embed_title', { guild: escapeMarkdown(ctx.guild.name) })
	return {
		embeds: [await buildEmbed(embedTitle, leadPlayers, ctx)],
		flags: MessageFlags.Ephemeral,
	}
}


async function getLeadPlayers(ctx: CmdContext, filterIds?: Snowflake[]): Promise<PlayerRankInfo[]> {
	if (filterIds?.length === 0) {
		return []
	}

	const players = await ctx.tx.createQueryBuilder()
		.select('*')
		.from(qb => {
			fromRankedPlayers(qb
				.select(['id', 'elo', 'anonymous'])
				.addSelect('rank() over (ORDER BY elo DESC)', 'rank')
			)
			if (filterIds !== undefined) {
				const idList = filterIds.map(id => `'${id}'`).join(', ')
				qb.andWhere(`id IN (${idList})`)
			}
			return qb
		}, 'T')
		.where('rank <= 10')
		.orderBy('rank')
		.getRawMany<{
			id: Snowflake,
			elo: number,
			anonymous: boolean,
			rank: string,
		}>()

	return players.map(p => ({
		...p,
		rank: parseInt(p.rank, 10),
	}))
}

async function buildEmbed(title: string, players: PlayerRankInfo[], ctx: CmdContext): Promise<APIEmbed> {
	const displayNames = await getDisplayNames(players.map(p => p.id), ctx)

	return new EmbedBuilder()
		.setTitle(title)
		.setFields(players.map(player => {
			const rankEmoji = getRankEmoji(player.rank)
			const displayName = player.anonymous ? italic(ctx.p.t('utils.anonymous')) : displayNames[player.id]
			return {
				name: `${rankEmoji} ${player.rank}. ${displayName}`,
				value: ctx.p.t('cmd.lead.embed_field.value', { elo: player.elo }),
			}
		}))
		.toJSON()
}

export function getRankEmoji(rank: number): string {
	switch (rank) {
		case 1: return '🥇'
		case 2: return '🥈'
		case 3: return '🥉'
		default: return '✨'
	}
}

export default command
