import { SlashCommandIntegerOption } from '@discordjs/builders'
import assert from 'assert/strict'
import { client } from '../../bot'
import { CmdContext } from '../../context'
import { APIInteractionResponseCallbackData, MessageFlags } from '../../discordApiTypes'
import { Status } from '../../orm/entities/Game'
import { checkGameArg, Command, CommandType } from '../command'

const command: Command = {
	name: 'claim',
	type: CommandType.GAME,
	usableIfBanned: false,
	requireExternalEmojis: true,

	buildOptions(builder, t) {
		builder.addIntegerOption(
			new SlashCommandIntegerOption()
				.setName('game_id')
				.setDescription(t('cmd.claim.game_id_description'))
				.setRequired(true)
		)
	},

	async exec(ctx: CmdContext): Promise<void> {
		await client.interactionReply(ctx, await getResponse(ctx))
	},
}

async function getResponse(ctx: CmdContext): Promise<APIInteractionResponseCallbackData> {
	const { game, error } = await checkGameArg(ctx)
	if (error !== null) {
		return {
			content: error,
			flags: MessageFlags.Ephemeral
		}
	}

	if (game.currentPlayer?.id === ctx.userId) {
		return {
			content: ctx.p.t('cmd.claim.your_turn'),
			flags: MessageFlags.Ephemeral
		}
	}

	if (game.status < Status.BOTH_PLAYERS_INITIALIZED) {
		return {
			content: ctx.p.t('cmd.claim.no_opponent'),
			flags: MessageFlags.Ephemeral
		}
	}

	if (game.archived) {
		return {
			content: ctx.p.t('game.archived'),
			flags: MessageFlags.Ephemeral
		}
	}

	assert(game.currentPlayer !== null)

	if (game.turn < 2) {
		return {
			content: ctx.p.t('cmd.claim.no_move'),
			flags: MessageFlags.Ephemeral
		}
	} else {
		assert(game.lastMoveDatetime !== null)
	}

	if (Date.now() - game.lastMoveDatetime.getTime() < 86400000 /* 24h */) {
		return {
			content: ctx.p.t('cmd.claim.wait'),
			flags: MessageFlags.Ephemeral
		}
	}

	return await game.silentForfeit(game.currentPlayer.id, ctx)
}

export default command
