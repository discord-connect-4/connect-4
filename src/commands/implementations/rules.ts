import { EmbedBuilder } from '@discordjs/builders'
import { client } from '../../bot'
import { CmdContext } from '../../context'
import { APIEmbedField, MessageFlags } from '../../discordApiTypes'
import { mentionCommand } from '../../utils'
import { Command, CommandType } from '../command'

const command: Command = {
	name: 'rules',
	type: CommandType.GAME,
	usableIfBanned: false,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		const cmdMentions: {[key: string]: string} = {}
		for (const cmdName of ['play', 'stats', 'lead', 'claim']) {
			cmdMentions[cmdName + '_cmd'] = mentionCommand(cmdName)
		}

		function buildParagraph(paragraphName: string): APIEmbedField {
			return {
				name: ctx.p.t(`cmd.rules.embed.${paragraphName}.title`),
				value: ctx.p.t(`cmd.rules.embed.${paragraphName}.text`, cmdMentions),
			}
		}

		const embed = new EmbedBuilder()
			.setTitle(ctx.p.t('cmd.rules.embed.title'))
			.setFields([
				buildParagraph('game'),
				buildParagraph('fair_play'),
				buildParagraph('ranking'),
			])

		await client.interactionReply(ctx, {
			embeds: [embed.toJSON()],
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
