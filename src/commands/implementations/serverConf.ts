import {
	ActionRowBuilder,
	EmbedBuilder,
	MessageActionRowComponentBuilder,
	ModalBuilder,
	SlashCommandSubcommandBuilder,
	StringSelectMenuBuilder,
	StringSelectMenuOptionBuilder,
	TextInputBuilder
} from '@discordjs/builders'
import assert from 'assert/strict'
import { client } from '../../bot'
import { config } from '../../config'
import { CmdContext, Context, MenuContext, ModalContext } from '../../context'
import {
	APIInteractionResponseCallbackData,
	APIModalInteractionResponseCallbackData,
	MessageFlags,
	PermissionFlagsBits,
	TextInputStyle
} from '../../discordApiTypes'
import { getLocaleName, getLocales, getTranslationProgress, translate } from '../../intl'
import { maxUrlLength } from '../../orm/entities/Preferences'
import { Command, CommandType } from '../command'

const cmdName = 'server-conf'
const langSubCmdName = 'language'
const imgSubCmdName  = 'images'

const command: Command = {
	name: cmdName,
	type: CommandType.MANAGEMENT,
	usableIfBanned: true,
	requireExternalEmojis: false,
	guildOnly: true,
	defaultMemberPerms: PermissionFlagsBits.ManageGuild,

	buildOptions(builder, t) {
		builder.addSubcommand(new SlashCommandSubcommandBuilder()
			.setName(langSubCmdName)
			.setDescription(t('cmd.server-conf.lang.description')))

		builder.addSubcommand(new SlashCommandSubcommandBuilder()
			.setName(imgSubCmdName)
			.setDescription(t('cmd.server-conf.img.description')))
	},

	async exec(ctx: CmdContext): Promise<void> {
		if (ctx.interaction.data.options?.find(o => o.name === langSubCmdName)) {
			await client.interactionReply(ctx, buildLangMenu(ctx))
		} else {
			await client.interactionShowModal(ctx.interaction, buildImgModal(ctx))
		}
	},

	async onMenuInput(ctx: MenuContext): Promise<void> {
		const newLocale = ctx.interaction.data.values[0]
		assert(getLocales().includes(newLocale))

		ctx.p.locale = newLocale
		await ctx.tx.save(ctx.p)

		await client.componentInteractionUpdateMsg(ctx.interaction, buildLangMenu(ctx))
	},

	async onModalSubmit(ctx: ModalContext): Promise<void> {
		const components = ctx.interaction.data.components
			.map(row => row.components)
			.reduce((a, b) => [...a, ...b])

		function getValue(customId: string): string | null {
			const component = components.find(c => c.custom_id === customId)
			assert(component !== undefined)
			return component.value === '' ? null : component.value
		}

		const chosenWinImgUrl = getValue('win_img')
		const chosenTieImgUrl = getValue('tie_img')

		ctx.p.winImgUrl = chosenWinImgUrl === null ? null : (isValidWebURL(chosenWinImgUrl) ? chosenWinImgUrl : null)
		ctx.p.tieImgUrl = chosenTieImgUrl === null ? null : (isValidWebURL(chosenTieImgUrl) ? chosenTieImgUrl : null)
		await ctx.tx.save(ctx.p)

		function getMsg(value: string | null): string {
			if (value === null) {
				return ctx.p.t('cmd.server-conf.img.default')
			}

			if (!isValidWebURL(value)) {
				return ctx.p.t('cmd.server-conf.img.invalid_url')
			}

			if (!value.match(/\.(?:png|jpg|jpeg|gif)$/i)) {
				return ctx.p.t('cmd.server-conf.img.not_img')
			}

			return ctx.p.t('cmd.server-conf.img.ok')
		}

		await client.interactionReply(ctx, {
			embeds: [
				new EmbedBuilder()
					.setTitle(ctx.p.t('cmd.server-conf.img.win_preview'))
					.setDescription(getMsg(chosenWinImgUrl))
					.setImage(ctx.p.effectiveWinImgUrl)
					.toJSON(),
				new EmbedBuilder()
					.setTitle(ctx.p.t('cmd.server-conf.img.tie_preview'))
					.setDescription(getMsg(chosenTieImgUrl))
					.setImage(ctx.p.effectiveTieImgUrl)
					.toJSON(),
			],
			flags: MessageFlags.Ephemeral,
		})
	},
}

function buildLangMenu(ctx: Context): APIInteractionResponseCallbackData {
	const langMenu = new StringSelectMenuBuilder().setCustomId('lang')

	for (const locale of getLocales()) {
		const progress = Math.round(getTranslationProgress(locale) * 100)

		const label = progress === 100
			? getLocaleName(locale)
			: translate(locale, 'cmd.server-conf.lang.translation_progress', { locale: getLocaleName(locale), progress })

		langMenu.addOptions(
			new StringSelectMenuOptionBuilder()
				.setValue(locale)
				.setLabel(label)
				.setDefault(locale === ctx.p.effectiveLocale)
		)
	}

	const embed = new EmbedBuilder().setTitle(ctx.p.t('cmd.server-conf.lang.title'))

	return {
		embeds: [embed.toJSON()],
		components: [
			new ActionRowBuilder<MessageActionRowComponentBuilder>()
				.addComponents(langMenu)
				.toJSON()
		],
		flags: MessageFlags.Ephemeral,
	}
}

function buildImgModal(ctx: CmdContext): APIModalInteractionResponseCallbackData {
	const winImgInput = new TextInputBuilder()
		.setCustomId('win_img')
		.setLabel(ctx.p.t('cmd.server-conf.img.win'))
		.setRequired(false)
		.setStyle(TextInputStyle.Short)
		.setPlaceholder(config.default_game_over_img.win)
		.setValue(ctx.p.winImgUrl ?? '')
		.setMaxLength(maxUrlLength)

	const tieImgInput = new TextInputBuilder()
		.setCustomId('tie_img')
		.setLabel(ctx.p.t('cmd.server-conf.img.tie'))
		.setRequired(false)
		.setStyle(TextInputStyle.Short)
		.setPlaceholder(config.default_game_over_img.tie)
		.setValue(ctx.p.tieImgUrl ?? '')
		.setMaxLength(maxUrlLength)

	return new ModalBuilder()
		.setTitle(ctx.p.t('cmd.server-conf.img.title'))
		.setCustomId(cmdName + '.images')
		.addComponents(new ActionRowBuilder<TextInputBuilder>().addComponents(winImgInput))
		.addComponents(new ActionRowBuilder<TextInputBuilder>().addComponents(tieImgInput))
		.toJSON()
}

function isValidWebURL(url: string): boolean {
	try {
		const proto = new URL(url).protocol
		return proto === 'http:' || proto === 'https:'
	} catch (_) {
		return false
	}
}

export default command
