import { ButtonBuilder } from '@discordjs/builders'
import { client, getBotInviteUrl } from '../../bot'
import { CmdContext } from '../../context'
import { ButtonStyle, MessageFlags } from '../../discordApiTypes'
import { Command, CommandType, organiseButtons } from '../command'

const command: Command = {
	name: 'invite',
	type: CommandType.UTILITY,
	usableIfBanned: true,
	requireExternalEmojis: false,

	async exec(ctx: CmdContext): Promise<void> {
		await client.interactionReply(ctx, {
			content: ctx.p.t('cmd.invite.text'),
			components: organiseButtons([
				new ButtonBuilder()
					.setStyle(ButtonStyle.Link)
					.setLabel(ctx.p.t('cmd.invite.btn_title'))
					.setURL(getBotInviteUrl())
			]),
			flags: MessageFlags.Ephemeral,
		})
	},
}

export default command
