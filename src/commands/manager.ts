import { SlashCommandBuilder } from '@discordjs/builders'
import assert from 'assert/strict'
import { APISelectMenuComponent, ComponentType, InteractionType } from 'discord-api-types/v10'
import { readdir } from 'fs/promises'
import path from 'path'
import { client } from '../bot'
import { config } from '../config'
import { BtnContext, CmdContext, ComponentContext, Context, MenuContext, ModalContext } from '../context'
import { RESTPostAPIChatInputApplicationCommandsJSONBody, Snowflake } from '../discordApiTypes'
import { translateIntoFallback } from '../intl'
import { mainLogger } from '../logger'
import { Ban } from '../orm/entities/Ban'
import { Command, CommandType } from './command'

interface CommandWrapper {
	command: Command
	slashCommandId: Snowflake
}

const commands: Record<string, CommandWrapper> = {}

export async function loadCommands() {
	const cmdDir = path.resolve(__dirname, 'implementations')
	const files = (await readdir(cmdDir))
		.filter(file => file.endsWith('.js')) // keep only JS files
		.map(file => `${cmdDir}/${file}`)     // use absolute path

	const modules: { default: Command }[] = await Promise.all(files.map(file => import(file)))
	const slashCommands = await client.fetchSlashCommands()

	for (const module of modules) {
		const command = module.default
		let slashCommandId = slashCommands.find(c => c.name == command.name)?.id
		if (slashCommandId === undefined) {
			mainLogger.info('Could not find a slash command matching the "%s" command. Registering a new slash command.', command.name)
			const registeredSlashCommand = await client.createSlashCommand(commandToSlashCommand(command))
			slashCommandId = registeredSlashCommand.id
		}
		commands[command.name] = { command, slashCommandId }
	}

	const unlinkedSlashCommands = slashCommands.filter(slashCommand =>
		!Object.values(commands).map(c => c.slashCommandId).includes(slashCommand.id)
	)

	for (const slashCommand of unlinkedSlashCommands) {
		mainLogger.info('Could not associate the slash command %s (name: %s) to a command. Unregistering it.', slashCommand.id, slashCommand.name)
		await client.deleteSlashCommand(slashCommand.id)
	}
}

function getCommandWrapper(name: string): CommandWrapper {
	const res = commands[name]
	if (res === undefined) {
		throw new Error(`command with name "${name}" not found`)
	}
	return res
}

export function getCommands(type: CommandType | null = null): Command[] {
	return Object.values(commands).map(c => c.command).filter(c => type === null || c.type === type)
}

export function getCommandsName(): string[] {
	return Object.keys(commands)
}

export function getCommand(name: string): Command {
	return getCommandWrapper(name).command
}

export function getSlashCommandId(name: string): Snowflake {
	return getCommandWrapper(name).slashCommandId
}

export async function updateSlashCommand(commandName: string) {
	await client.modifySlashCommand(getSlashCommandId(commandName), commandToSlashCommand(getCommand(commandName)))
}

export async function updateSlashCommands() {
	await client.bulkOverwriteSlashCommands(getCommands().map(commandToSlashCommand))
}

function commandToSlashCommand(command: Command): RESTPostAPIChatInputApplicationCommandsJSONBody {
	const builder = new SlashCommandBuilder()
		.setName(command.name)
		.setDescription(translateIntoFallback(`cmd.${command.name}.description`))

	if (command.guildOnly) {
		builder.setDMPermission(false)
	}

	if (command.defaultMemberPerms !== undefined) {
		builder.setDefaultMemberPermissions(command.defaultMemberPerms)
	}

	if (command.buildOptions !== undefined) {
		command.buildOptions(builder, translateIntoFallback)
	}

	return builder.toJSON()
}

export async function processCommandInteraction(ctx: CmdContext): Promise<void> {
	const command = getCommand(ctx.interaction.data.name)

	const banMsg = await getBanMsgIfAny(command, ctx)
	if (banMsg !== null){
		await client.interactionReply(ctx, { content: banMsg })
		return
	}

	if (command.requireExternalEmojis && ctx.guild !== null && !ctx.guild.hasEveryoneExtEmojisPerm(ctx.interaction.channel_id)) {
		ctx.logger.info('Skipping the interaction because of the missing UseExternalEmojis permission.')
		await client.interactionReply(ctx, {
			content:
				ctx.p.t('cmd_manager.missing_emojis_perm', { everyone: '@everyone' }) + '\n'
				+ ctx.p.t('cmd_manager.denied_perm.' + ctx.guild.getExtEmojisPermDenyOrigin(ctx.interaction.channel_id)),
			allowed_mentions: { parse: [] },
		})
		return
	}

	ctx.logger.info(`executing command ${ctx.interaction.data.name}`)
	await command.exec(ctx)
}

export async function processComponentInteraction(ctx: ComponentContext): Promise<void> {
	const originalInteraction = ctx.interaction.message.interaction
	assert(originalInteraction !== undefined)

	const commandName = originalInteraction.name.split(' ')[0]
	const command = getCommand(commandName)

	const banMsg = await getBanMsgIfAny(command, ctx)
	if (banMsg !== null) {
		await client.componentInteractionUpdateMsg(ctx.interaction, { content: banMsg })
		return
	}

	if (ctx.interaction.data.component_type === ComponentType.Button) {
		ctx.logger.info(`executing button ${ctx.interaction.data.custom_id} for command ${commandName}`)
		assert(command.onBtnClicked !== undefined)
		await command.onBtnClicked(ctx as BtnContext)

	} else if (isMenu(ctx.interaction.data.component_type)) {
		ctx.logger.info(`executing menu ${ctx.interaction.data.custom_id} for command ${commandName}`)
		assert(command.onMenuInput !== undefined)
		await command.onMenuInput(ctx as MenuContext)
	}
}

export async function processModalInteraction(ctx: ModalContext): Promise<void> {
	const commandName = ctx.interaction.data.custom_id.split('.')[0]
	const command = getCommand(commandName)

	ctx.logger.info(`executing modal ${ctx.interaction.data.custom_id}`)
	assert(command.onModalSubmit !== undefined)
	await command.onModalSubmit(ctx)
}

async function getBanMsgIfAny(command: Command, ctx: Context): Promise<string | null> {
	if (command.usableIfBanned) {
		return null
	}

	const ban = await ctx.tx.findOne(Ban, ctx.userId)
	if (!ban) {
		return null
	}

	let msg = ctx.p.t('cmd_manager.banned.msg') + '\n' + ban.reason
	if (config.support_server) {
		msg += '\n' + ctx.p.t('cmd_manager.banned.contest', { contestUrl: `https://discord.gg/${config.support_server.invite_code}` })
	}
	return msg
}

function isMenu(componentType: ComponentType): componentType is APISelectMenuComponent['type'] {
	return [
		ComponentType.StringSelect,
		ComponentType.UserSelect,
		ComponentType.RoleSelect,
		ComponentType.MentionableSelect,
		ComponentType.ChannelSelect,
	].includes(componentType)
}
