import { prompt } from 'inquirer'
import { Snowflake } from '../discordApiTypes'
import { adminToolsSocketPath, Backend } from './backend'
import { connectToIPCServer, disconnectFromIPCServer } from './ipc'

let adminToolsCore: Backend
cli().catch(console.error)

async function cli() {
	adminToolsCore = await connectToIPCServer(adminToolsSocketPath, Backend)

	let running = true
	while (running) {
		const commands = {
			'ban': ban,
			'unban': unban,
			'toggle-maintenance': toggleMaintenance,
			'reload-a-command': reloadCommand,
			'reload-all-commands': reloadAllCommands,
			'pull-translations': pullTranslations,
			'give-translator-disc': giveTranslatorDisc,
			'give-gems': giveGems,
			'exit': () => running = false,
		}

		const { command } = await prompt<{ command: keyof typeof commands }>([
			{
				type: 'list',
				message: 'exec',
				name: 'command',
				choices: Object.keys(commands),
			},
		])

		await commands[command]()
	}

	await disconnectFromIPCServer(adminToolsSocketPath)
	process.exit()
}

async function ban(): Promise<void> {
	const userId = await promptUserId()
	if (userId === null) {
		printError('Could not find this user.')
		return
	}

	const alreadyBanned = await adminToolsCore.isUserBanned(userId)
	if (alreadyBanned) {
		printError(`This user is already banned for the following reason:\n${alreadyBanned}`)
		return
	}

	const { reason } = await prompt<{ reason: string }>([
		{
			type: 'input',
			message: 'reason',
			name: 'reason',
		},
	])

	await adminToolsCore.banUser(userId, reason)
	printSuccess('User banned.')
}

async function unban(): Promise<void> {
	const userId = await promptUserId()
	if (userId === null) {
		printError('Could not find this user.')
		return
	}

	const banned = await adminToolsCore.isUserBanned(userId)
	if (!banned) {
		printError('This user isn\'t banned.')
		return
	}

	await adminToolsCore.unbanUser(userId)
	printSuccess('User unbanned.')
}

async function toggleMaintenance(): Promise<void> {
	const maintenanceEnabled = await adminToolsCore.toggleMaintenance()
	printSuccess(`Maintenance mode ${maintenanceEnabled ? 'enabled' : 'disabled'}.`)
}

async function reloadCommand(): Promise<void> {
	const commandsName = await adminToolsCore.getCommandsName()
	const { commandName } = await prompt([
		{
			type: 'list',
			message: 'command',
			name: 'commandName',
			choices: commandsName,
		},
	])
	await adminToolsCore.reloadACommand(commandName)
	printSuccess(`${commandName} command reloaded.`)
}

async function reloadAllCommands(): Promise<void> {
	await adminToolsCore.reloadAllCommands()
	printSuccess(`Slash commands reloaded.`)
}

async function pullTranslations(): Promise<void> {
	await adminToolsCore.pullTranslation()
	printSuccess(`Translations reloaded.`)
}

async function giveTranslatorDisc() {
	const userId = await promptUserId()
	if (userId === null) {
		printError('Could not find this user.')
		return
	}
	await adminToolsCore.giveTranslatorDisc(userId)
	printSuccess(`Disc given.`)
}

async function giveGems() {
	const userId = await promptUserId()
	if (userId === null) {
		printError('Could not find this user.')
		return
	}
	const { quantity } = await prompt<{ quantity: number }>([
		{
			type: 'number',
			message: 'quantity',
			name: 'quantity',
			validate: (input: number) => input > 0 && Math.floor(input) === input
		}
	])
	await adminToolsCore.giveGems(userId, quantity)
	printSuccess('Gems given.')
}

async function promptUserId(): Promise<Snowflake | null> {
	const { userId } = await prompt<{ userId: string }>([
		{
			type: 'input',
			message: 'user ID',
			name: 'userId',
			validate: input => !!input.match(/^\d+$/) ?? 'syntax error'
		},
	])

	if (await adminToolsCore.doesUserExist(userId)) {
		return userId
	} else {
		return null
	}
}

function printSuccess(msg: string) {
	console.log(`\x1b[32m➜ ${msg}\x1b[0m`)
}

function printError(msg: string) {
	console.log(`\x1b[31m➜ ${msg}\x1b[0m`)
}
