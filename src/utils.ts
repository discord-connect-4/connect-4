import { chatInputApplicationCommandMention, escapeMarkdown, italic } from '@discordjs/builders'
import { DiscordAPIError } from '@discordjs/rest'
import baseX from 'base-x'
import { spawn } from 'child_process'
import { client } from './bot'
import { getSlashCommandId } from './commands/manager'
import { Context } from './context'
import { RESTJSONErrorCodes, RESTPostAPIChannelMessageJSONBody, Snowflake } from './discordApiTypes'

export type PrimitiveType = string | number | boolean | null | undefined | Date

export const base3 = baseX('012')
export const base10 = baseX('0123456789')

export async function parallelForEach<T>(array: T[], func: (item: T) => Promise<any>): Promise<void> {
	await Promise.all(array.map(func))
}

export async function asyncFilter<T>(array: T[], filter: (item: T) => Promise<boolean>): Promise<T[]> {
	const predicates = await Promise.all(array.map(filter))
	return array.filter((_, index) => predicates[index])
}

export async function getDisplayNames(userIds: Snowflake[], ctx: Context): Promise<Record<Snowflake, string>> {
	userIds = [...new Set(userIds)] // remove duplicates
	const displayNames: Record<Snowflake, string> = {}

	await parallelForEach(userIds, async userId => {
		displayNames[userId] = await getDisplayName(userId, ctx)
	})

	return displayNames
}

export async function getDisplayName(userId: Snowflake, ctx: Context): Promise<string> {
	if (ctx.guild !== null) {
		try {
			const member = await client.fetchGuildMember(ctx.guild.id, userId)
			return escapeMarkdown(member.nick ?? member.user.username)
		} catch (e) {
			// too bad, username will be used instead of nickname
		}
	}

	try {
		const { username, discriminator } = await client.fetchUser(userId)
		return escapeMarkdown(username + '#' + discriminator)
	} catch (err) {
		if (err.httpStatus !== 404) {
			ctx.logger.warn(err)
		}
		return italic(ctx.p.t('utils.unknown_user'))
	}
}

export function mentionCommand(cmdName: string): string
export function mentionCommand(cmdName: string, subCmdName: string): string
export function mentionCommand(cmdName: string, subCmdName?: string): string {
	if (subCmdName == undefined) {
		return chatInputApplicationCommandMention(cmdName, getSlashCommandId(cmdName))
	} else {
		return chatInputApplicationCommandMention(cmdName, subCmdName, getSlashCommandId(cmdName))
	}
}

export function formatNumber(n: number, addSign: boolean = false): string {
	const negative = n < 0
	if (negative) n = -n

	let formatted = chunks(n.toString().split(''), 3).map(x => x.join('')).join(' ')

	if (negative) {
		formatted = '-' + formatted
	} else if (addSign) {
		formatted = '+' + formatted
	}

	return formatted
}

function chunks<T>(array: T[], chunkSize: number): T[][] {
	const result: T[][] = []

	for (let i = array.length; i > 0; i -= chunkSize) {
		result.unshift(array.slice(Math.max(0, i - chunkSize), i))
	}

	return result
}

export async function trySendDM(userId: Snowflake, msg: RESTPostAPIChannelMessageJSONBody) {
	try {
		await client.sendDM(userId, msg)
	} catch(e) {
		if ((e as DiscordAPIError).code === RESTJSONErrorCodes.CannotSendMessagesToThisUser) {
			// ignore error (user has blocked his DM)
		} else {
			throw e
		}
	}
}

export function compareIds(a: Snowflake, b: Snowflake): number {
	let res = a.length - b.length
	return res !== 0 ? res : a.localeCompare(b)
}

export function intersectSorted<T>(a: T[], b: T[], compareFunc: (a: T, b: T) => number): T[] {
	const res: T[] = []

	let aIdx = 0
	let bIdx = 0

	const aLen = a.length
	const bLen = b.length

	while (aIdx < aLen && bIdx < bLen) {
		const cmp = compareFunc(a[aIdx], b[bIdx])

		if (cmp === 0) {
			res.push(a[aIdx])
			++aIdx
			++bIdx
		} else if (cmp < 0) {
			++aIdx
		} else {
			++bIdx
		}
	}

	return res
}

export function shallowDump(raw: Record<string, any>, maxDepth: number, curDepth: number = 0): string {
	let res = '{\n'

	for (const key in raw) {
		const value = raw[key]
		const type = typeof value
		let strValue: string
		if (type === 'string' || type === 'number' || value === null) {
			strValue = JSON.stringify(value)
		} else if (type === 'undefined') {
			strValue = 'undefined'
		} else if (type === 'object') {
			if  (curDepth + 1 === maxDepth) {
				continue
			}
			strValue = shallowDump(value, maxDepth, curDepth + 1)
			if (strValue === '{}') {
				continue
			}
		} else {
			continue
		}
		res += ' '.repeat(4 * (curDepth + 1)) + key + ': ' + strValue + '\n'
	}

	if (res === '{\n') {
		res = '{}'
	} else {
		res += ' '.repeat(4 * curDepth) + '}'
	}

	return res
}

export function exec(command: string, args: string[], stdin: Buffer): Promise<void> {
	return new Promise<void>((resolve, reject) => {
		const proc = spawn(command, args)
		let stderr = Buffer.alloc(0)
		proc.on('error', reject)
		proc.on('exit', (code, signal) => {
			if (code === null) {
				reject(`The process was terminated with the "${signal}" signal. stderr:\n${stderr.toString()}`)
			} else if (code !== 0) {
				reject(`The process exited with the "${code}" exit code. stderr:\n${stderr.toString()}`)
			} else {
				resolve()
			}
		})
		proc.stderr.on('data', (chunk: Buffer) => stderr = Buffer.concat([stderr, chunk]))
		proc.stdin.write(stdin, err => {
			if (err !== null && err !== undefined) {
				reject(err)
			}
		})
		proc.stdin.end()
	})
}
