import assert from 'assert/strict'
import { BinaryLike, createHash } from 'crypto'
import { Logger } from 'log4js'
import { EntityManager, getConnection } from 'typeorm'
import { client } from './bot'
import {
	APIChatInputApplicationCommandInteraction,
	APIInteraction,
	APIMessageComponentButtonInteraction,
	APIMessageComponentInteraction,
	APIMessageComponentSelectMenuInteraction,
	APIModalSubmitInteraction,
	GatewayInteractionCreateDispatchData,
	Snowflake
} from './discordApiTypes'
import { GuildInfo } from './guilds'
import { getLogger } from './logger'
import { getPreferences, Preferences } from './orm/entities/Preferences'
import { base10 } from './utils'

export interface Context<T extends APIInteraction = APIInteraction> {
	name: string
	interaction: T
	guild: GuildInfo | null
	userId: Snowflake
	tx: EntityManager
	p: Preferences
	logger: Logger
}

export type CmdContext = Context<APIChatInputApplicationCommandInteraction>
export type ModalContext = Context<APIModalSubmitInteraction>
export type ComponentContext = Context<APIMessageComponentInteraction>
export type BtnContext = Context<APIMessageComponentButtonInteraction>
export type MenuContext = Context<APIMessageComponentSelectMenuInteraction>

export function execInContext(interaction: GatewayInteractionCreateDispatchData, operations: (ctx: Context) => Promise<void>): Promise<void> {
	return getConnection().transaction(async tx => {
		const p = await getPreferences(interaction.guild_id ?? null, tx);
		const ctxName = hash(base10.decode(interaction.id)) + '/' + hash(Int32Array.of(Math.random() * 1_000_000))

		const user = interaction.user ?? interaction.member?.user;
		assert(user !== undefined)

		const guild = (() => {
			if (interaction.guild_id === undefined) {
				return null
			}
			const res = client.guilds.get(interaction.guild_id)
			assert(res !== undefined)
			return res
		})()

		const ctx = {
			name: ctxName,
			interaction: interaction,
			guild,
			userId: user.id,
			tx: tx,
			p: p,
			logger: getLogger(ctxName),
		}

		await operations(ctx)
	})
}

function hash(str: BinaryLike): string {
	return createHash('md5').update(str).digest('base64url').substring(0, 4)
}
