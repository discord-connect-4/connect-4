import { RGBTuple } from '@discordjs/builders'
import assert from 'assert/strict'
import { APIMessageComponentEmoji } from './discordApiTypes'

export enum Currency {
	COIN,
	GEM,
	PRICELESS, // priceless discs are manually given and can't be bought
}

export interface Disc {
	index: number,
	emoji: `<${'a' | ''}:${string}:${number}>`,
	price: number,
	currency: Currency,
	embedColor: number | RGBTuple,
}

export function emojiForBtn(disc: Disc): APIMessageComponentEmoji {
	let emoji = disc.emoji

	const match = emoji.match(/<(a?):[a-z0-9_]+:(\d{18})>/)
	assert(match !== null)

	return {
		id: match[2],
		animated: match[1] === 'a',
	}
}

let index = 0;
let _translatorDiscIndex: number

export const discs: Disc[] = [
	{
		index: index++,
		emoji: '<:4:955044649041526794>', // red circle
		price: 0,
		currency: Currency.COIN,
		embedColor: [227, 43, 70],
	},
	{
		index: index++,
		emoji: '<:4:955047134829359215>', // yellow circle
		price: 0,
		currency: Currency.COIN,
		embedColor: [255, 202, 106],
	},
	{
		index: index++,
		emoji: '<:4:955047134682550292>', // blue circle
		price: 400,
		currency: Currency.COIN,
		embedColor: [79, 173, 233],
	},
	{
		index: index++,
		emoji: '<:4:955047134711914496>', // brown circle
		price: 400,
		currency: Currency.COIN,
		embedColor: [197, 104, 83],
	},
	{
		index: index++,
		emoji: '<:4:955047134720294962>', // purple circle
		price: 400,
		currency: Currency.COIN,
		embedColor: [173, 143, 209],
	},
	{
		index: index++,
		emoji: '<:4:955047134716121088>', // green circle
		price: 400,
		currency: Currency.COIN,
		embedColor: [115, 177, 100],
	},
	{
		index: index++,
		emoji: '<:4:955047134661591080>', // radio button
		price: 600,
		currency: Currency.COIN,
		embedColor: [107, 163, 196],
	},
	{
		index: index++,
		emoji: '<:4:955047134653218826>', // soccer
		price: 800,
		currency: Currency.COIN,
		embedColor: 0xbcc0c0,
	},
	{
		index: index++,
		emoji: '<:4:955047134703550545>', // basketball
		price: 800,
		currency: Currency.COIN,
		embedColor: [248, 143, 51],
	},
	{
		index: index++,
		emoji: '<:4:955047134640631808>', // tennis
		price: 800,
		currency: Currency.COIN,
		embedColor: [103, 178, 97],
	},
	{
		index: index++,
		emoji: '<:4:955047134632226836>', // volleyball
		price: 800,
		currency: Currency.COIN,
		embedColor: 0xbcc0c0,
	},
	{
		index: index++,
		emoji: '<:4:955047134485426188>', // radioactive sign
		price: 1500,
		currency: Currency.COIN,
		embedColor: [248, 143, 51],
	},
	{
		index: index++,
		emoji: '<:4:955047134640615445>', // globe with meridians
		price: 1500,
		currency: Currency.COIN,
		embedColor: [53, 137, 191],
	},
	{
		index: index++,
		emoji: '<:4:955047134628020264>', // snowflake
		price: 3000,
		currency: Currency.COIN,
		embedColor: [128, 195, 236],
	},
	{
		index: index++,
		emoji: '<:4:955047134623858738>', // cyclone
		price: 3000,
		currency: Currency.COIN,
		embedColor: [79, 173, 233],
	},
	{
		index: index++,
		emoji: '<:4:955047134435090484>', // cd
		price: 5000,
		currency: Currency.COIN,
		embedColor: 0x99aab5,
	},
	{
		index: index++,
		emoji: '<:4:955047134930038854>', // dvd
		price: 10000,
		currency: Currency.COIN,
		embedColor: 0xf1c40f,
	},
	{
		index: index++,
		emoji: '<a:4:919968935661686815>', // red spinning disc
		price: 1,
		currency: Currency.GEM,
		embedColor: [255, 0, 0],
	},
	{
		index: index++,
		emoji: '<a:4:919968934831222807>', // yellow spinning disc
		price: 1,
		currency: Currency.GEM,
		embedColor: [255, 255, 0],
	},
	{
		index: index++,
		emoji: '<a:4:919968935959466035>', // cyan spinning disc
		price: 2,
		currency: Currency.GEM,
		embedColor: [0, 255, 255],
	},
	{
		index: index++,
		emoji: '<a:4:919968925092036658>', // spinning diamond
		price: 10,
		currency: Currency.GEM,
		embedColor: [157, 228, 242],
	},
	{
		index: _translatorDiscIndex = index++,
		emoji: '<a:4:938118403019902976>', // spinning earth
		price: NaN,
		currency: Currency.PRICELESS,
		embedColor: [30, 79, 212],
	},
]

assert(discs.length <= 25, 'All discs can\'t fit in a 5*5 button array.')
assert(discs.length <= 32, 'All discs can\'t fit in 32-bit bitfield.')

export const translatorDiscIndex = _translatorDiscIndex
