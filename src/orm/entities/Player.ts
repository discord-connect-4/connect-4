import { Column, Entity, EntityManager, PrimaryColumn, SelectQueryBuilder } from 'typeorm'
import { Currency, Disc } from '../../disc'
import { Snowflake } from '../../discordApiTypes'
import { Outcome, outcomeToScore } from '../../gameUtils'

export interface StatsUpdate {
	deltaElo: number,
	newElo: number,
	deltaCoins: number,
	newCoins: number,
}

export function fromRankedPlayers(qb: SelectQueryBuilder<any>): SelectQueryBuilder<Player> {
	return qb
		.from(Player, 'Player')
		.where('loses + ties + wins > 0')
		.andWhere('id NOT IN (SELECT "userId" FROM "Ban")')
}

@Entity('Player')
export class Player {
	@PrimaryColumn({ type: 'bigint', nullable: false })
	public id: Snowflake
	@Column({ type: 'timestamp with time zone', nullable: false })
	public registrationDatetime = new Date()
	@Column({ type: 'int', nullable: false })
	public elo: number
	@Column({ type: 'boolean', nullable: false, default: false })
	public isMaster: boolean
	@Column({ type: 'int', nullable: false, default: 0 })
	public loses: number
	@Column({ type: 'int', nullable: false, default: 0 })
	public ties: number
	@Column({ type: 'int', nullable: false, default: 0 })
	public wins: number
	@Column({ type: 'boolean', nullable: false, default: false })
	public anonymous: boolean
	@Column({ type: 'int', nullable: false, default: 0b11 })
	public discs: number
	@Column({ type: 'smallint', nullable: false, default: 0 })
	public mainDisc: number
	@Column({ type: 'smallint', nullable: false, default: 1 })
	public secondaryDisc: number
	@Column({ type: 'int', nullable: false, default: 0 })
	public coins: number
	@Column({ type: 'int', nullable: false, default: 0 })
	public gems: number
	@Column({ type: 'timestamp with time zone', nullable: true })
	public lastVoteDatetime: Date | null
	@Column({ type: 'int', nullable: false, default: 0})
	public voteStrike: number

	public static async createFromId(id: Snowflake, transaction: EntityManager): Promise<Player> {
		const player = new Player()
		player.id = id
		player.elo = await Player.getAvgElo(transaction)
		await transaction.save(player)
		return player
	}

	public static async createIfDoesNotExist(id: Snowflake, tx: EntityManager): Promise<Player> {
		let player = await tx.findOne(Player, id)
		if (!player) {
			player = await Player.createFromId(id, tx)
		}
		return player
	}

	private static async getAvgElo(transaction: EntityManager): Promise<number> {
		const { avgElo } = await transaction.createQueryBuilder()
			.select('AVG(elo)', 'avgElo')
			.from(Player, 'Player')
			.getRawOne() as { avgElo: string }

		return avgElo ? Math.round(parseFloat(avgElo)) : 1000
	}

	public getExpectedScore(opponent: Player): number {
		return 1 / (1 + Math.pow(10, (opponent.elo - this.elo) / 400))
	}

	public processGameOutcome(expectedScore: number, outcome: Outcome) {
		const statsUpdate = this.updateEloAndCoins(expectedScore, outcome)
		this.updateGameCount(outcome)
		return statsUpdate
	}

	public chooseDisc(opponentDisc: number): number {
		return this.mainDisc === opponentDisc ? this.secondaryDisc : this.mainDisc
	}

	public get nbOfPlayedGames(): number {
		return this.loses + this.ties + this.wins
	}

	public get winRate(): number {
		return this.wins / this.nbOfPlayedGames
	}

	public haveDisc(disc: Disc): boolean {
		return !!(this.discs >> disc.index & 1)
	}

	public buyDisc(disc: Disc): boolean {
		if (this.haveDisc(disc)) {
			return false
		}

		switch (disc.currency) {
		case Currency.COIN:
			if (disc.price > this.coins) {
				return false
			}
			this.coins -= disc.price
			break

		case Currency.GEM:
			if (disc.price > this.gems) {
				return false
			}
			this.gems -= disc.price
			break

		case Currency.PRICELESS:
			return false
		}

		this.giveDisc(disc.index)
		return true
	}

	public giveDisc(discIndex: number) {
		this.discs |= 1 << discIndex
	}

	private updateEloAndCoins(expectedScore: number, outcome: Outcome): StatsUpdate {
		const actualScore = outcomeToScore(outcome)
		const kFactor = this.getKFactor()

		const deltaElo = Math.round(kFactor * (actualScore - expectedScore))
		const deltaCoins = Math.max(0, Math.round(100 * (actualScore - expectedScore)))

		this.elo += deltaElo
		this.coins += deltaCoins

		if (this.elo >= 2400) {
			this.isMaster = true
		}

		return {
			deltaElo,
			newElo: this.elo,
			deltaCoins,
			newCoins: this.coins,
		}
	}

	private getKFactor(): number {
		if (this.nbOfPlayedGames <= 30) {
			return this.elo < 2300 ? 40 : 20
		} else {
			return this.isMaster ? 10 : 20
		}
	}

	private updateGameCount(outcome: Outcome) {
		switch (outcome) {
			case Outcome.LOOSE:
				++this.loses
				break

			case Outcome.TIE:
				++this.ties
				break

			case Outcome.WIN:
				++this.wins
				break
		}
	}
}
