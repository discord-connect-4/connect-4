import { Webhook, WebhookPayload } from '@top-gg/sdk'
import assert from 'assert/strict'
import express from 'express'
import superagent from 'superagent'
import { getConnection } from 'typeorm'
import { client } from './bot'
import { config } from './config'
import { getLogger, mainLogger } from './logger'
import { Player } from './orm/entities/Player'
import { formatNumber, trySendDM } from './utils'

const voteHookLogger = getLogger('vote hook')
const statsPosterLogger = getLogger('stats poster')

export async function startTopggIntegration() {
	if (!config.topgg) {
		mainLogger.info('Not using top.gg integration.')
		return
	}

	// post bot stats to top.gg every 30 minutes
	if (config.dev_mode) {
		mainLogger.info('top.gg integration: not using autoPoster')
	} else {
		setInterval(() => postStats().catch(e => statsPosterLogger.error(e)), 30 * 60 * 1000)
		mainLogger.info('top.gg integration: autoPoster is ready')
	}

	// listen for user vote
	await setupWebHook()
}

async function setupWebHook(): Promise<any> {
	assert(config.topgg)

	const port = config.dev_mode ? 8080 : 80
	const app = express()
	const webhook = new Webhook(config.topgg.webhook_auth)

	app.post('/topgg-webhook', webhook.listener(onVote))

	await new Promise<void>(resolve => {
		app.listen(port, () => {
			mainLogger.info(`Web hook server is listening on port ${port}.`)
			resolve()
		})
	})
}

async function onVote(vote: WebhookPayload) {
	if (vote.type !== 'upvote') {
		if (vote.type === 'test') {
			voteHookLogger.info('Test vote received.')
			voteHookLogger.info(vote)
		}
		return
	}

	voteHookLogger.info(`User with ID ${vote.user} just voted!`)

	const { reward, nextReward } = await getConnection().transaction(async tx => {
		// get player
		const player = await Player.createIfDoesNotExist(vote.user, tx)
		const now = new Date()

		// update lastVoteDatetime and voteStrike
		if (player.lastVoteDatetime && now.getTime() - player.lastVoteDatetime.getTime() < 172800000 || isSubjectToChristmasBug(player.lastVoteDatetime)) { // 48 hours
			++player.voteStrike
		} else {
			player.voteStrike = 0
		}
		player.lastVoteDatetime = now

		// reward player
		const multiplier = 200
		const reward = (player.voteStrike + 1) * multiplier
		player.coins += reward

		// save player
		await tx.save(player)

		return {
			reward: reward,
			nextReward: (player.voteStrike + 2) * multiplier,
		}
	})

	// thank user
	await trySendDM(vote.user, {
		content: `Thanks for supporting me!\nHere 🪙 ${formatNumber(reward)} for you. Vote again tomorrow to get 🪙 ${formatNumber(nextReward)} more!`
	})
}

async function postStats() {
	assert(config.topgg)

	await superagent
		.post(`https://top.gg/api/bots/${client.user.id}/stats`)
		.set('Authorization', config.topgg.token)
		.send({
			server_count: client.guilds.size,
			shard_count: client.shardCount,
		})
}

/**
 * A bug occurred from 2021-12-25 to 2022-01-24 where vote where no longer registered by the bot.
 * This function aims to keep the vote strike valid.
 *
 * The cause of the bug was a domain name change without updating the webhook URL.
 */
function isSubjectToChristmasBug(lastVoteDatetime: Date | null): boolean {
	if (lastVoteDatetime === null) {
		return false
	}

	const d = lastVoteDatetime.getUTCDate()
	const m = lastVoteDatetime.getUTCMonth() + 1
	const y = lastVoteDatetime.getUTCFullYear()

	return y === 2021 && m === 12 && (d === 24 || d === 25)
}
